import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException
    {
        int recordsNum;
        String region;

        if (Check.isArgsCorrect(args))
        {
            recordsNum = Integer.parseInt(args[0]);
            region = args[1];

            FakeDataGenerator.generateFakersInCSV(region, recordsNum);
        }
    }
}
