import com.github.javafaker.Faker;
import com.opencsv.CSVWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class FakeDataGenerator 
{   
    public static void generateFakersInCSV(String region, int recordsNum) 
            throws IOException       
    {
        Faker faker;
        List<String[]> fakers = new ArrayList<>();
        
        faker = createFakerByRegion(region);
        
        for (int i = 0; i < recordsNum; i++)
        {
            fakers.add(generateFakerData(faker));       
        }
        writeInCSVFormat(fakers);
    }
       
    public static void writeInCSVFormat(List<String[]> fakers) 
            throws IOException 
    {                 
        PrintWriter pw = new PrintWriter(System.out);
        
        CSVWriter writer = new CSVWriter(
                pw, ';', CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
        
        writer.writeAll(fakers);
        writer.close();
    }
    
    public static String[] generateFakerData(Faker faker)            
    {    
        return new String[]{             
        faker.name().firstName(),
        faker.name().lastName(),
        faker.address().state(),
        faker.address().city(),
        faker.address().streetName(),
        faker.address().zipCode(),
        faker.phoneNumber().phoneNumber()};
    }
	
    public static Faker createFakerByRegion(String region)          
    {                          
        
        if (region.equals("en"))
            return new Faker(new Locale("en_US"));
        else if (region.equals("ru"))
            return new Faker(new Locale("ru"));
        else if (region.equals("uk"))
            return new Faker(new Locale("uk_UA"));
        else
            return new Faker(new Locale("by"));

    }

    public static void main(String[] args) throws IOException 
    {  
        int recordsNum;        
        String region;
        
        if (Check.isArgsCorrect(args))
        {
            recordsNum = Integer.parseInt(args[0]);
            region = args[1];
            
            generateFakersInCSV(region, recordsNum);            
        }
    }
}
