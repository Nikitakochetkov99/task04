public class Check 
{
   public static boolean isNumeric(String str) 
    { 
        try 
        {  
            Integer.parseInt(str);  
            return true;
        } 
        catch(NumberFormatException e)
        {  
            return false;  
        }  
}
    
    public static void exampleUsage()
    {
        System.out.println("""
                           
                    Usage:
                    --------------------------
                    java Generator [recordsNumber] [region]                   
                    Avaliable regions: en, ru, uk, by                        
                    You must enter at least 1 record       
                           """);
    }
    
    public static boolean isArgsCorrect(String[] args)
    {
        int recordsNum;
        
        if (args.length < 2)
        {
            System.out.printf("Error: number of arguments are wrong"
                    + "(expected 2, found %d)", args.length);
            exampleUsage();
            
            return false;
        }        
        else if(!isNumeric(args[0]))
        {
            System.out.println("Error: can't read the number of records");
            exampleUsage();
            
            return false;
        }        
        else if (Integer.parseInt(args[0]) <= 0)
        {
            System.out.println("Error: the number of records are wrong");
            exampleUsage();
            
            return false;
        } 
        else if (!args[1].equals("en") && !args[1].equals("ru") 
                && !args[1].equals("uk") && !args[1].equals("by"))
        {
            System.out.println("Error: name of the region is wrong");
            exampleUsage();
            
            return false;
        }
        else
            return true;
            
    } 
}
